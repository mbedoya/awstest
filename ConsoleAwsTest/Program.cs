﻿using Common.AWS.DynamoDB;
using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAwsTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Starting...");

                Console.WriteLine("Getting all accounts...");
                DynamoService dbService = new DynamoService();
                IEnumerable<Account> accounts = dbService.GetAll<Account>();

                if (accounts.Count() == 0)
                {
                    Console.WriteLine("No accounts found, creating...");
                    AccountMaker.FillUp();
                }

                Console.WriteLine(accounts.Count());

                Console.WriteLine("Getting specific account...");
                Account account = dbService.GetItem<Account>("2");

                Console.WriteLine(account.Email);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();


        }
    }
}
