﻿using Common.AWS.DynamoDB;
using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAwsTest
{
    public class AccountMaker
    {
        public static void FillUp()
        {
            DynamoService dbService = new DynamoService();
            for (int i = 0; i < 10; i++)
            {
                Account newAccount = new Account()
                {
                    Id = i.ToString(),
                    Name = "Peter " + i.ToString(),
                    Email = "peter." + i.ToString() + "gmail.com",
                    DateCreated = DateTime.Now
                };

                dbService.Store<Account>(newAccount);
            }
        }
    }
}
