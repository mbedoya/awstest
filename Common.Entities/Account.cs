﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    [DynamoDBTable("account")]
    public class Account
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        //[DynamoDBRangeKey]
        public DateTime DateCreated { get; set; }
        public string ParentAccountId { get; set; }
    }
}
