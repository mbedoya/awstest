﻿using Common.AWS.DynamoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Common.Entities;

namespace WebAWSTest.Controllers
{
    public class AccountController : ApiController
    {
        // GET: api/Account
        public IEnumerable<Account> Get()
        {
            DynamoService dbService = new DynamoService();
            return dbService.GetAll<Account>();
        }

        // GET: api/Account/5
        public Account Get(int id)
        {
            DynamoService dbService = new DynamoService();
            return dbService.GetItem<Account>(id.ToString());
        }

        // POST: api/Account
        public void Post([FromBody]Account value)
        {
        }

        // PUT: api/Account/5
        public void Put(int id, [FromBody]Account value)
        {
        }

        // DELETE: api/Account/5
        public void Delete(int id)
        {

        }
    }
}
